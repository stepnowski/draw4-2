import java.awt.Graphics;
import javax.swing.JPanel;

public class DrawPanel extends JPanel
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// paintComponent needed with every JPanel
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);//1st statement needed with paintComponent
		
		int width = getWidth(); //public method of JPanel
		int height = getHeight(); // public method of JPanel
		int i=0;
		
		while(i <15)
		{	
			//top left corner start
			//draw a line from the upper left to the bottom left but in one step of 1/15th the width
			//next line down one step of 1/15th to the bottom left but in two steps of 1/15th the width
			g.drawLine(0, i*height/15,(i+1)*(width/15),height);
			
			//bottom left corner start
			g.drawLine(i*width/15,height,width,((height)-((i+1)*(height/15))));
			
			//bottom right corner start
			g.drawLine(width,((height)-((i)*(height/15))),((width)-((i+1)*(width/15))),0);
			
			//top right corner start
			g.drawLine(((width)-((i)*(width/15))),0,0,(i+1)*(width/15));
				
			i++;
		}
		
		
	}
}
